﻿using static System.Console;

namespace CSharp6._0
{
    public class NullCheck
    {
        public Customer NewCustomer { get; set; }
        public void Run()
        {
            PopulateClass();


            WriteLine("Legacy Style");
            if (NewCustomer != null && NewCustomer.PrimaryAddress != null && NewCustomer.PrimaryAddress.Line1 != null)
            {
                WriteLine("Address is " + NewCustomer.PrimaryAddress.Line1);
            }
            else
            {
                WriteLine("No Address Found.");
            }
            WriteLine("");
            WriteLine("New Style");

            var address = NewCustomer?.PrimaryAddress?.Line1;
            WriteLine(address != null ? "Address is " + address : "No Address Found.");
            
            ReadLine();
        }

        private void PopulateClass()
        {
            NewCustomer = new Customer();
            NewCustomer.Name = "John";
        }
    }
}
