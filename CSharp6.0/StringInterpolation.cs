﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace CSharp6._0
{
    public class StringInterpolation
    {
        public Customer NewCustomer { get; set; }
        public void Run()
        {
            PopulateClass();

            WriteLine("Legacy Style");
            WriteLine(string.Format("Customer {0} resides at {1} {2} in {3}", 
                NewCustomer.Name, 
                NewCustomer.PrimaryAddress.Line1, 
                NewCustomer.PrimaryAddress.Line2, 
                NewCustomer.PrimaryAddress.City));
            WriteLine();
            WriteLine("New Style");
            var address = NewCustomer.PrimaryAddress;
            WriteLine($"Customer {NewCustomer.Name} resides at {address.Line1} {address.Line2} in {address.City}");
            ReadLine();
        }

        private void PopulateClass()
        {
            NewCustomer = new Customer();
            NewCustomer.Name = "John";
            NewCustomer.PrimaryAddress = new Address();
            NewCustomer.PrimaryAddress.Line1 = "QSI Healthcare, 4th Floor";
            NewCustomer.PrimaryAddress.Line1 = "Block 14, Pritech Park, Bellandur";
            NewCustomer.PrimaryAddress.City = "Bangalore";
        }
    }
}
