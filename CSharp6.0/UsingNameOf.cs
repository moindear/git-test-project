﻿using System;
using static System.Console;

namespace CSharp6._0
{
    public class UsingNameOf
    {
        public Customer NewCustomer { get; set; }
        public void Run()
        {
            try
            {
                if (NewCustomer == null) throw new ArgumentNullException("NewCustomer");
            }
            catch (Exception ex)
            {
                WriteLine(ex);
            }

            WriteLine("-----------------------------------------------------");

            try
            {
                if (NewCustomer == null) throw new ArgumentNullException(nameof(NewCustomer));
            }
            catch (Exception ex)
            {
                WriteLine(ex);
                ReadLine();
            }
            
        }
    }
}
