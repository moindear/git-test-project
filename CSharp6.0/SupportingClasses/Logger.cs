﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp6._0
{
    
    public class Logger
    {
        public async Task LogAsync(string message)
        {

            using (StreamWriter writer = new StreamWriter(@"C:\logger.log",true))
            {
                await writer.WriteLineAsync(message);
            }

            return;

        }
    }
}
