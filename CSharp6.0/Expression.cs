﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp6._0
{
    public class Expression
    {
        public void Run()
        {
            Console.WriteLine("Legacy Style");
            Console.WriteLine("Name is " + GetName("Moin","Pasha"));
            Console.WriteLine("");
            Console.WriteLine("New Style");
            Console.WriteLine("Name is " + GetName6("Moin","Pasha"));
        }

        private string GetName(string firstName, string lastName)
        {
            return firstName + " " + lastName;
        }

        private string GetName6(string firstName, string lastName) => firstName + " " + lastName;
    }
}
