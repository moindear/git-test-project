﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp6._0
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 1;

            var autoProperties = new AutoProperties();
            var expression = new Expression();
            var usingStatic = new UsingStatic();
            var nullCheck = new NullCheck();
            var stringInterpolation = new StringInterpolation();
            var usingNameOf = new UsingNameOf();
            var collectionInitializer = new CollectionInitializer();
            var conditionalCatching = new ConditionalCatching();
            var awaitCatching = new AwaitCatching();

            //Comment
            autoProperties.Run();
            expression.Run();
            //usingStatic.Run();
            //nullCheck.Run();
            //stringInterpolation.Run();
            //usingNameOf.Run();
            //collectionInitializer.Run();
            //conditionalCatching.Run();
            //Task.Run(()=> awaitCatching.Run());
            //shashi
        }
    }
}
