﻿using System;
using static System.Console;

namespace CSharp6._0
{
    public class UsingStatic
    {
        public void Run()
        {
            Console.WriteLine("Legacy Style");
            Console.WriteLine("Sample Text to print");
            WriteLine("Sample Text to print");

            ReadLine();
        }
    }
}
