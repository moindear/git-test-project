﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp6._0
{
    public class AutoProperties
    {
        public string _name = "Moin";
        public string _city = "Bangalore";
        public string Name { get; set; } = "Moin";

        public string City { get; } = "Bangalore";
        public string NewName
        {
            get { return _name; }
            set { _name = value; }
        }

        public string NewCity
        {
            get { return _city; }
        }

        public void Run()
        {
            //City = "Bombay";
            //NewCity = "Bombay";

            Console.WriteLine("Legacy Style");
            Console.WriteLine("Name is " + NewName);
            Console.WriteLine("");
            Console.WriteLine("New Style");
            Console.WriteLine("Name is " + Name);
            
            Console.ReadLine();
        }
    }
}
