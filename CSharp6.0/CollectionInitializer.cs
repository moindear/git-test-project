﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace CSharp6._0
{
    public class CollectionInitializer
    {
        public void Run()
        {
            WriteLine("Legacy Style");
            var dictionary = new Dictionary<int, string>();
            dictionary[7] = "sete";
            dictionary[9] = "nove";
            dictionary[13] = "treze";
            PrintDictionary(dictionary);
            WriteLine();
            WriteLine("New Style");
            var numbers = new Dictionary<int, string>
            {
                [7] = "sete",
                [9] = "nove",
                [13] = "treze"
            };
            PrintDictionary(numbers);

            ReadLine();
        }

        private void PrintDictionary(Dictionary<int, string> dictionary)
        {
            foreach (var entry in dictionary)
            {
                WriteLine(entry);
            }
        }
    }
}
