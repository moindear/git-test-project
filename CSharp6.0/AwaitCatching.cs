﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
using System.Data.Sql;
using System.Data.SqlClient;

namespace CSharp6._0
{
    public class AwaitCatching
    {
        public async Task Run()
        {
            var logger = new Logger();
            var connectionString = "server=BLRVDWEBAPISQL0;database=NextGenMobile;user id=sa;pwd=nextgen1";
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(connectionString);
                await connection.OpenAsync();
            }
            catch (Exception ex)
            {
                WriteLine("Error");
                await logger.LogAsync(ex.Message);
                throw;
            }

            ReadLine();
        }
    }
}
