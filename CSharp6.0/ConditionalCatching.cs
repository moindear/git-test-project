﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace CSharp6._0
{
    public class ConditionalCatching
    {
        public void Run()
        {
            bool errorHandling = true;
            try
            {
                int x = 0;
                int y = 5/x;
            }
            catch (Exception ex) when (ex.Source == "CSharp6.01")
            {
                WriteLine("Filtered for the source CSharp6.01");
                WriteLine(ex.Message);
            }
            catch (Exception exc)
            {
                WriteLine("Filtered for the other source");
                WriteLine(exc.Message);
            }

            ReadLine();
        }
    }
}
